﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneHelper : MonoBehaviour
{
    public void OnSceneNext(string title)
    {
        SceneManager.LoadScene(title, LoadSceneMode.Single);
    }
}
