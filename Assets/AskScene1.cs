﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class AskScene1 : MonoBehaviour
{
    // private static bool[] ask_scene1 = new bool[10] { false, false, false, false, false, false, false, false, false, false };
    private string[] ask_scene2 = new string[10] { "", "", "", "", "", "", "", "", "", "" };
    public static USER_ASK _USER_ASK;

    public Text[] dropdown_text = new Text[10];

    public void AskScene1_1_Set(int value)
    {
        _USER_ASK = new USER_ASK();

        _USER_ASK.CHOISE_1_1 = Convert.ToBoolean(value);
        SceneManager.LoadScene("AskScene_2", LoadSceneMode.Single);
    }

    public void AskScene1_2_Set(int value)
    {
        _USER_ASK.CHOISE_1_2 = Convert.ToBoolean(value);
        SceneManager.LoadScene("AskScene_3", LoadSceneMode.Single);
    }

    public void AskScene1_3_Set(int value)
    {
        _USER_ASK.CHOISE_1_3 = Convert.ToBoolean(value);
        SceneManager.LoadScene("AskScene_4", LoadSceneMode.Single);
    }

    public void AskScene1_4_Set(int value)
    {
        _USER_ASK.CHOISE_1_4 = Convert.ToBoolean(value);
        SceneManager.LoadScene("AskScene_5", LoadSceneMode.Single);
    }

    public void AskScene1_5_Set(int value)
    {
        _USER_ASK.CHOISE_1_5 = Convert.ToBoolean(value);
        SceneManager.LoadScene("AskScene_6", LoadSceneMode.Single);
    }

    public void AskScene1_6_Set(int value)
    {
        _USER_ASK.CHOISE_1_6 = Convert.ToBoolean(value);
        SceneManager.LoadScene("AskScene_7", LoadSceneMode.Single);
    }

    public void AskScene1_7_Set(int value)
    {
        _USER_ASK.CHOISE_1_7 = Convert.ToBoolean(value);
        SceneManager.LoadScene("AskScene_8", LoadSceneMode.Single);
    }

    public void AskScene1_8_Set(int value)
    {
        _USER_ASK.CHOISE_1_8 = Convert.ToBoolean(value);
        SceneManager.LoadScene("AskScene_9", LoadSceneMode.Single);
    }

    public void AskScene1_9_Set(int value)
    {
        _USER_ASK.CHOISE_1_9 = Convert.ToBoolean(value);
        SceneManager.LoadScene("AskScene_10", LoadSceneMode.Single);
    }

    public void AskScene1_10_Set(int value)
    {
        _USER_ASK.CHOISE_1_10 = Convert.ToBoolean(value);

        int score1 = 0;

        if (!AskScene1._USER_ASK.CHOISE_1_1) //
            score1 += 1;
        if (!AskScene1._USER_ASK.CHOISE_1_2) //
            score1 += 1;
        if (AskScene1._USER_ASK.CHOISE_1_3) //
            score1 += 1;
        if (AskScene1._USER_ASK.CHOISE_1_4) //
            score1 += 1;
        if (AskScene1._USER_ASK.CHOISE_1_5) //
            score1 += 1;
        if (!AskScene1._USER_ASK.CHOISE_1_6) //
            score1 += 1;
        if (AskScene1._USER_ASK.CHOISE_1_7) //
            score1 += 1;
        if (!AskScene1._USER_ASK.CHOISE_1_8) //
            score1 += 1;
        if (AskScene1._USER_ASK.CHOISE_1_9) //
            score1 += 1;
        if (AskScene1._USER_ASK.CHOISE_1_10) //
            score1 += 1;

        AskScene1._USER_ASK.SCORE_1 = score1;

        SceneManager.LoadScene("Score1", LoadSceneMode.Single);
    }

    public void GoToAskMain3()
    {
        SceneManager.LoadScene("AskMain3", LoadSceneMode.Single);
    }

    public void AskScene2_Set()
    {
        if (_USER_ASK == null)
            _USER_ASK = new USER_ASK();

        for (int i = 0; i < 10; i++)
        {
            ask_scene2[i] = dropdown_text[i].text;
        }

        _USER_ASK.CHOISE_2_1 = ask_scene2[0];
        _USER_ASK.CHOISE_2_2 = ask_scene2[1];
        _USER_ASK.CHOISE_2_3 = ask_scene2[2];
        _USER_ASK.CHOISE_2_4 = ask_scene2[3];
        _USER_ASK.CHOISE_2_5 = ask_scene2[4];
        _USER_ASK.CHOISE_2_6 = ask_scene2[5];
        _USER_ASK.CHOISE_2_7 = ask_scene2[6];
        _USER_ASK.CHOISE_2_8 = ask_scene2[7];
        _USER_ASK.CHOISE_2_9 = ask_scene2[8];
        _USER_ASK.CHOISE_2_10 = ask_scene2[9];
    }
}
