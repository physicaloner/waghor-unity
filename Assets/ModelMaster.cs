﻿using System;

[Serializable]
public class USER_ASK
{
    public int? ID;

    public bool CHOISE_1_1;
    public bool CHOISE_1_2;
    public bool CHOISE_1_3;
    public bool CHOISE_1_4;
    public bool CHOISE_1_5;
    public bool CHOISE_1_6;
    public bool CHOISE_1_7;
    public bool CHOISE_1_8;
    public bool CHOISE_1_9;
    public bool CHOISE_1_10;

    public string CHOISE_2_1;
    public string CHOISE_2_2;
    public string CHOISE_2_3;
    public string CHOISE_2_4;
    public string CHOISE_2_5;
    public string CHOISE_2_6;
    public string CHOISE_2_7;
    public string CHOISE_2_8;
    public string CHOISE_2_9;
    public string CHOISE_2_10;

    public int CHOISE_3_1;
    public int CHOISE_3_2;
    public int CHOISE_3_3;
    public int CHOISE_3_4;
    public int CHOISE_3_5;
    public int CHOISE_3_6;
    public int CHOISE_3_7;
    public int CHOISE_3_8;
    public int CHOISE_3_9;
    public int CHOISE_3_10;
    public int CHOISE_3_11;
    public int CHOISE_3_12;
    public int CHOISE_3_13;
    public int CHOISE_3_14;
    public int CHOISE_3_15;
    public int CHOISE_3_16;
    public int CHOISE_3_17;
    public int CHOISE_3_18;
    public int CHOISE_3_19;
    public int CHOISE_3_20;

    public int SCORE_1;
    public int SCORE_2;
    public int SCORE_3;

    public DateTime UPDATE_DATE;
}

[Serializable]
public class USER_INFO
{
    public int? ID;
    public int CHOISE_1;
    public int CHOISE_2;
    public int CHOISE_3;
    public int CHOISE_4;
    public int CHOISE_5;
    public int CHOISE_6;
    public int CHOISE_7;
    public int CHOISE_8;
    public int CHOISE_9;
    public int CHOISE_10;
    public int CHOISE_11;
    public int CHOISE_12;
    public int CHOISE_13;
    public int CHOISE_14;
    public int CHOISE_15;
    public int CHOISE_16;
    public int CHOISE_17;
    public int CHOISE_18;
    public int CHOISE_19;
    public int CHOISE_20;
    public int CHOISE_21;
    public int SCORE;

    public DateTime UPDATE_DATE;
}
