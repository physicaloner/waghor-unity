﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class KeySender : MonoBehaviour
{
    public Text textName;
    public Text textEmail;

    public static string txtName;
    public static string txtEmail;

    public void KeyStart()
    {
        System.Diagnostics.Process.Start("osk.exe");
    }

    public void SendEmail()
    {
        txtName = textName.text;
        txtEmail = textEmail.text;
        SceneManager.LoadScene("ScreenCapture", LoadSceneMode.Single);
    }
}
