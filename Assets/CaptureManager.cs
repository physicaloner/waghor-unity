﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;

public class CaptureManager : MonoBehaviour
{
    public Text textName;
    public Text textDay;
    public Text textMonth;
    public Text textYear;
    public Button btnNext;

    private string take_name = "image_taken.jpg";

    // Start is called before the first frame update
    void Start()
    {
        textName.text = KeySender.txtName;
        textDay.text = System.DateTime.Now.Day.ToString();
        textMonth.text = GetMonth(System.DateTime.Now.Month);
        textYear.text = (System.DateTime.Now.Year + 543).ToString();

        if (System.IO.File.Exists($"C:\\waghor_data\\{take_name}"))
            System.IO.File.Delete($"C:\\waghor_data\\{take_name}");
    }

    private string GetMonth(int month)
    {
        switch(month)
        {
            case 1:
                {
                    return "มกราคม";
                }
            case 2:
                {
                    return "กุมภาพันธ์";
                }
            case 3:
                {
                    return "มีนาคม";
                }
            case 4:
                {
                    return "เมษายน";
                }
            case 5:
                {
                    return "พฤษภาคม";
                }
            case 6:
                {
                    return "มิถุนายน";
                }
            case 7:
                {
                    return "กรกฎาคม";
                }
            case 8:
                {
                    return "สิงหาคม";
                }
            case 9:
                {
                    return "กันยายน";
                }
            case 10:
                {
                    return "ตุลาคม";
                }
            case 11:
                {
                    return "พฤศจิกายน";
                }
            case 12:
                {
                    return "ธันวาคม";
                }
            default:
                {
                    return "-";
                }
        }
    }

    public void SendEmailNow()
    {
        //take_name = $"image_{System.DateTime.Now.Day}-{System.DateTime.Now.Month}-{System.DateTime.Now.Year}_{System.DateTime.Now.Ticks}.png";

        btnNext.gameObject.SetActive(false);

        if (!System.IO.Directory.Exists(@"C:\waghor_data"))
            System.IO.Directory.CreateDirectory(@"C:\waghor_data");

        ScreenCapture.CaptureScreenshot($"C:\\waghor_data\\{take_name}");
        Debug.Log("Save file success.");

        StartCoroutine(delayedShare($"C:\\waghor_data\\{take_name}"));

        //SendEmail_WithMail();
        //SceneManager.LoadScene("EndMain", LoadSceneMode.Single);
    }

    IEnumerator delayedShare(string path)
    {
        while (IsFileUnavailable(path))
        {
            Debug.Log("file locked");
            yield return new WaitForSeconds(.05f);
        }

        SendEmail_WithMail();     
    }

    protected virtual bool IsFileUnavailable(string path)
    {
        if (!File.Exists(path))
            return true;

        FileInfo file = new FileInfo(path);
        FileStream stream = null;

        try
        {
            stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
        }
        catch (IOException)
        {
            return true;
        }
        finally
        {
            if (stream != null)
                stream.Close();
        }

        return false;
    }

    private void SendEmail_WithMail()
    {
        var fromAddress = new MailAddress("allstar.aloner@gmail.com", "พิพิธภัณฑ์สัตว์น้ำ หว้ากอ");
        var toAddress = new MailAddress(KeySender.txtEmail, KeySender.txtName);
        const string fromPassword = "Allstaraloner09";
        const string subject = "เอกสารเกียรติบัตรจากการตอบคำถาม";
        const string body = "";

        var smtp = new SmtpClient
        {
            Host = "smtp.gmail.com",
            Port = 587,
            EnableSsl = true,
            DeliveryMethod = SmtpDeliveryMethod.Network,
            UseDefaultCredentials = false,
            Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
        };
        using (var message = new MailMessage(fromAddress, toAddress)
        {
            Subject = subject,
            Body = body
        })
        {           
            LinkedResource LinkedImage = new LinkedResource($"C:\\waghor_data\\{take_name}");
            LinkedImage.ContentId = "MyPic";
            LinkedImage.ContentType = new ContentType(MediaTypeNames.Image.Jpeg);

            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(
  "เกียรติบัตรนี้สำหรับผู้ที่ตอบคำถามได้คะแนนเกิน 80% ขึ้นไป, หากรูปภาพไม่ขึ้น กรุณาติดต่อเจ้าหน้าที่ค่ะ. <img src=cid:MyPic>",
  null, "text/html");

            htmlView.LinkedResources.Add(LinkedImage);
            message.AlternateViews.Add(htmlView);

            smtp.Send(message);
            Debug.Log("Sended email !");
            SceneManager.LoadScene("EndMain", LoadSceneMode.Single);
        }
    }
}
