﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using System.Linq;  
using System.Net;

public class AskChoiseTrigger : MonoBehaviour
{
    public static int _indexChoise = 1;
    public static int[] _answer = new int[20] { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
    public Text _textChoise;
    public Text _text1;
    public Text _text2;
    public Text _text3;
    public Text _text4;

    public Button btn1;
    public Button btn2;
    public Button btn3;
    public Button btn4;

    public void OnClickNext(int index)
    {
        try { _answer[_indexChoise] = index; }
        catch (System.Exception ex) { Debug.Log($"Save {_indexChoise} = {index} Error !"); }
        finally { _indexChoise += 1; }
        LoadNewChoise();
    }

    public void OnRemoveCursor()
    {
        EventSystem.current.SetSelectedGameObject(null);
    }

    public void LoadNewChoise()
    {
        switch (_indexChoise)
        {
            case 1:
                {
                    _textChoise.text = "1. ปลาชนิดใดอาศัยอยู่ในบริเวณแหล่งต้นน้ำ ลำธาร"; 
                    _text1.text = "   ก. ปลาแรด";
                    _text2.text = "   ข. ปลาพลวงหิน"; //
                    _text3.text = "   ค. ปลาสวาย";
                    _text4.text = "   ง. ปลายี่สก";
                    break;
                }
            case 2:
                {
                    _textChoise.text = "2. ปลาชนิดใดต่อไปนี้มีรูปร่างต่างจากชนิดอื่น";
                    _text1.text = "   ก. ปลาตะเพียนทอง";
                    _text2.text = "   ข. ปลากระสูบขีด";
                    _text3.text = "   ค. ปลาสร้อยนกเขา";
                    _text4.text = "   ง. ปลากะสง"; //
                    break;
                }
            case 3:
                {
                    _textChoise.text = "3. ปลาชนิดใดจัดเป็นปลาหายากใกล้สูญพันธุ์";
                    _text1.text = "   ก. ปลาลิ้นหมา";
                    _text2.text = "   ข. ปลากะพงแดง";
                    _text3.text = "   ค. ปลากระทุงเหว";
                    _text4.text = "   ง. ปลากระโห้"; //
                    break;
                }
            case 4:
                {
                    _textChoise.text = "4. ปลาชนิดใดจัดเป็นปลาอันตรายต่อมนูษย์";
                    _text1.text = "   ก. ปลาเสือตอ";
                    _text2.text = "   ข. ปลาสิงโต"; //
                    _text3.text = "   ค. ปลาเสือพ่นน้ำ";
                    _text4.text = "   ง. ปลามุกประดิษฐ์";
                    break;
                }
            case 5:
                {
                    _textChoise.text = "5. ปลาชนิดใดมีขนาดเล็กที่สุด";
                    _text1.text = "   ก. ปลาฉลาม";
                    _text2.text = "   ข. ปลากระเบน";
                    _text3.text = "   ค. ปลาสวาย";
                    _text4.text = "   ง. ปลาก้างพระร่วง"; //
                    break;
                }
            case 6:
                {
                    _textChoise.text = "6. ปลาที่มีการเปลี่ยนแปลงเกล็ดเป็นเกราะหุ้มลำตัวคือ";
                    _text1.text = "   ก. ปลาสร้อยขาว";
                    _text2.text = "   ข. ปลาฉลาม";
                    _text3.text = "   ค. ปลาค้างคาว";
                    _text4.text = "   ง. ปลาจิ้มฟันจระเข้"; //
                    break;
                }
            case 7:
                {
                    _textChoise.text = "7. ปลาขนาดเล็กที่คอยเก็บกินปรสิตของปลาขนาดใหญ่คือ";
                    _text1.text = "   ก. ปลาม้าน้ำ";
                    _text2.text = "   ข. ปลาพยาบาล"; //
                    _text3.text = "   ค. ปลาการ์ตูน";
                    _text4.text = "   ง. ปลาซิว";
                    break;
                }
            case 8:
                {
                    _textChoise.text = "8. ระบบนิเวศน์ของจังหวัดประจวบคีรีขันธ์เป็นแบบใด";
                    _text1.text = "   ก. ป่าดิบชื้น";
                    _text2.text = "   ข. ป่าดิบเขา";
                    _text3.text = "   ค. ป่าผลัดใบ"; //
                    _text4.text = "   ง. ป่าชายหาด";
                    break;
                }
            case 9:
                {
                    _textChoise.text = "9. หอยชนิดใดมีขนาดใหญ่ที่สุดในโลก";
                    _text1.text = "   ก. หอยแครง";
                    _text2.text = "   ข. หอยแมลงภู่";
                    _text3.text = "   ค. หอยมือเสือ"; //
                    _text4.text = "   ง. หอยโข่งทะเล";
                    break;
                }
            case 10:
                {
                    _textChoise.text = "10. หอยที่มักพบตามโขดหินริมทะเลคือ";
                    _text1.text = "   ก. หอยนางรม"; //
                    _text2.text = "   ข. หอยแครง";
                    _text3.text = "   ค. หอยกระพง";
                    _text4.text = "   ง. หอยสังข์หนาม";
                    break;
                }
            case 11:
                {
                    _textChoise.text = "11. ปลาชนิดใดจัดเป็นปลาที่อาศัยใต้หินน้ำ";
                    _text1.text = "   ก. ปลากระเบน";
                    _text2.text = "   ข. ปลาเสือพ่นน้ำ";
                    _text3.text = "   ค. ปลาค้างคาว"; //
                    _text4.text = "   ง. ปลานกแก้ว";
                    break;
                }
            case 12:
                {
                    _textChoise.text = "12. สัตว์ทะเลใดเป็นศัตรูกับปะการังธรรมชาติ";
                    _text1.text = "   ก. ปลิงทะเล";
                    _text2.text = "   ข. ดาวมงกุฎหนาม"; //
                    _text3.text = "   ค. ดาวขนนก";
                    _text4.text = "   ง. ดาวหมอนปักเข็ม";
                    break;
                }
            case 13:
                {
                    _textChoise.text = "13. ข้อใดคือปลากะรังที่มีขนาดใหญ่ที่สุด";
                    _text1.text = "   ก. ปลาหมอทะเล"; //
                    _text2.text = "   ข. ปลากะรังหางซ้อน";
                    _text3.text = "   ค. ปลากะรังแดง";
                    _text4.text = "   ง. ปลากะรังหน้างอน";
                    break;
                }
            case 14:
                {
                    _textChoise.text = "14. อวัยวะส่วนใดของแมงดาถ้วยและแมงดาจานที่แตกต่างกัน";
                    _text1.text = "   ก. กระดอง";
                    _text2.text = "   ข. ท้อง";
                    _text3.text = "   ค. ตา";
                    _text4.text = "   ง. หาง"; //
                    break;
                }
            case 15:
                {
                    _textChoise.text = "15. สัตว์น้ำชนิดใดจับเหยื่อโดยใช้เข็มพิษแทง";
                    _text1.text = "   ก. ปลาสิงโต";
                    _text2.text = "   ข. ปูเสฉวนขน";
                    _text3.text = "   ค. แมงกะพรุน"; //
                    _text4.text = "   ง. ไม่มีข้อถูก";
                    break;
                }
            case 16:
                {
                    _textChoise.text = "16. โลกของเราเกิดขึ้นได้อย่างไร";
                    _text1.text = "   ก. การรวมตัวของกลุ่มก๊าซร้อน"; //
                    _text2.text = "   ข. การรวมตัวของลาวา";
                    _text3.text = "   ค. การแยกตัวจากดาวอังคาร";
                    _text4.text = "   ง. การรวมตัวของอุกกาบาต";
                    break;
                }
            case 17:
                {
                    _textChoise.text = "17. บรรยากาศเริ่มแรกของโลกเป็นอย่างไร";
                    _text1.text = "   ก. หนาวเย็นเป็นน้ำแข็ง";
                    _text2.text = "   ข. อากาศร้อนรุนแรง"; //
                    _text3.text = "   ค. มีถ้ำและภูเขาไฟ";
                    _text4.text = "   ง. มีทะเลทราย";
                    break;
                }
            case 18:
                {
                    _textChoise.text = "18. สิ่งมีชีวิตแรกที่เกิดบนโลกคืออะไร";
                    _text1.text = "   ก. พืชน้ำ";
                    _text2.text = "   ข. ชีวิตเซลล์เดียวอย่างง่าย"; //
                    _text3.text = "   ค. แพลงก์ตอน";
                    _text4.text = "   ง. มนุษย์วานร";
                    break;
                }
            case 19:
                {
                    _textChoise.text = "19. สิ่งมีชีวิตใดอาศัยอยู่บริเวณป่าชายเลน";
                    _text1.text = "   ก. ปลาตีน"; //
                    _text2.text = "   ข. ปลาเก๋า";
                    _text3.text = "   ค. ปลาหนวดพราหมณ์";
                    _text4.text = "   ง. ปลาดุก";
                    break;
                }
            case 20:
                {
                    _textChoise.text = "20. การทำประมงที่ถูกวิธีคือข้อใด";
                    _text1.text = "   ก. การใช้ไฟฟ้าช็อต";
                    _text2.text = "   ข. การใช้ยาเบื่อเมา";
                    _text3.text = "   ค. การใช้ระเบิด";
                    _text4.text = "   ง. การใช้อวนล้อม"; //
                    break;
                }
            default:
                {
                    //_indexChoise = 1;
                    //_answer = new int[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                    AskScene3_Set();
                    //SceneManager.LoadScene("MainScene", LoadSceneMode.Single);
                    break;
                }
        }
    }

    public void AskScene3_Set()
    {
        if(AskScene1._USER_ASK == null)
            AskScene1._USER_ASK = new USER_ASK();

        AskScene1._USER_ASK.CHOISE_3_1 = _answer[0];
        AskScene1._USER_ASK.CHOISE_3_2 = _answer[1];
        AskScene1._USER_ASK.CHOISE_3_3 = _answer[2];
        AskScene1._USER_ASK.CHOISE_3_4 = _answer[3];
        AskScene1._USER_ASK.CHOISE_3_5 = _answer[4];
        AskScene1._USER_ASK.CHOISE_3_6 = _answer[5];
        AskScene1._USER_ASK.CHOISE_3_7 = _answer[6];
        AskScene1._USER_ASK.CHOISE_3_8 = _answer[7];
        AskScene1._USER_ASK.CHOISE_3_9 = _answer[8];
        AskScene1._USER_ASK.CHOISE_3_10 = _answer[9];
        AskScene1._USER_ASK.CHOISE_3_11 = _answer[10];
        AskScene1._USER_ASK.CHOISE_3_12 = _answer[11];
        AskScene1._USER_ASK.CHOISE_3_13 = _answer[12];
        AskScene1._USER_ASK.CHOISE_3_14 = _answer[13];
        AskScene1._USER_ASK.CHOISE_3_15 = _answer[14];
        AskScene1._USER_ASK.CHOISE_3_16 = _answer[15];
        AskScene1._USER_ASK.CHOISE_3_17 = _answer[16];
        AskScene1._USER_ASK.CHOISE_3_18 = _answer[17];
        AskScene1._USER_ASK.CHOISE_3_19 = _answer[18];
        AskScene1._USER_ASK.CHOISE_3_20 = _answer[19];

        AskScene1._USER_ASK.UPDATE_DATE = System.DateTime.Now;

        btn1.enabled = false;
        btn2.enabled = false;
        btn3.enabled = false;
        btn4.enabled = false;

        int score3 = 0;

        if (AskScene1._USER_ASK.CHOISE_3_4 == 2)
            score3 += 1;
        if (AskScene1._USER_ASK.CHOISE_3_5 == 4)
            score3 += 1;
        if (AskScene1._USER_ASK.CHOISE_3_6 == 4)
            score3 += 1;
        if (AskScene1._USER_ASK.CHOISE_3_7 == 2)
            score3 += 1;
        if (AskScene1._USER_ASK.CHOISE_3_8 == 3)
            score3 += 1;
        if (AskScene1._USER_ASK.CHOISE_3_9 == 3)
            score3 += 1;
        if (AskScene1._USER_ASK.CHOISE_3_10 == 1)
            score3 += 1;
        if (AskScene1._USER_ASK.CHOISE_3_11 == 3)
            score3 += 1;
        if (AskScene1._USER_ASK.CHOISE_3_12 == 2)
            score3 += 1;
        if (AskScene1._USER_ASK.CHOISE_3_13 == 1)
            score3 += 1;
        if (AskScene1._USER_ASK.CHOISE_3_14 == 4)
            score3 += 1;
        if (AskScene1._USER_ASK.CHOISE_3_15 == 3)
            score3 += 1;
        if (AskScene1._USER_ASK.CHOISE_3_16 == 1)
            score3 += 1;
        if (AskScene1._USER_ASK.CHOISE_3_17 == 2)
            score3 += 1;
        if (AskScene1._USER_ASK.CHOISE_3_18 == 2)
            score3 += 1;
        if (AskScene1._USER_ASK.CHOISE_3_19 == 1)
            score3 += 1;
        if (AskScene1._USER_ASK.CHOISE_3_20 == 4)
            score3 += 1;

        AskScene1._USER_ASK.SCORE_3 = score3;

        AskSceneAll_POST();
    }

    public void AskSceneAll_POST()
    {
        StartCoroutine(PostUser_WWW());
        //SceneManager.LoadScene("MainScene", LoadSceneMode.Single);
    }

    private IEnumerator PostUser_WWW()
    {
        // Convert object to json here
        var jsonString = JsonUtility.ToJson(AskScene1._USER_ASK);
        IPAddress[] ipaddress = Dns.GetHostAddresses("WAGHOR-SERVER");  
        IPAddress ip4 = ipaddress.Where(ip => ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork && ip.ToString().IndexOf("192") != -1).FirstOrDefault();
        var url = $"http://{ip4.ToString()}:5000/api/USER_ASK/CREATE";
        //var url = "http://localhost:5000/api/USER_ASK/CREATE";
        var request = new UnityWebRequest(url, "POST");
        byte[] bodyRaw = System.Text.Encoding.ASCII.GetBytes(jsonString);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");

        yield return request.SendWebRequest();

        Debug.Log("Status Code: " + request.responseCode);
        //AskScene1._USER_ASK = new USER_ASK();

        SceneManager.LoadScene("Score3", LoadSceneMode.Single);
    }

    public void CalculateScore80()
    {
        int avg = 0;

        avg += (AskScene1._USER_ASK.SCORE_1 * 10) / 2;
        avg += (AskScene1._USER_ASK.SCORE_3 * 5) / 2;

        if (avg >= 80)
            SceneManager.LoadScene("EmailSender", LoadSceneMode.Single);
        else
            SceneManager.LoadScene("EndNot80", LoadSceneMode.Single);
    }
}
