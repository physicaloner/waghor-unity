﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AskChoiseLoader : MonoBehaviour
{
    public Text _textChoise;
    public Text _text1;
    public Text _text2;
    public Text _text3;
    public Text _text4;

    // Start is called before the first frame update
    void Start()
    {
        _textChoise.text = "1. ปลาชนิดใดอาศัยอยู่ในบริเวณแหล่งต้นน้ำ ลำธาร";
        _text1.text = "   ก. ปลาแรด";
        _text2.text = "   ข. ปลาพลวงหิน";
        _text3.text = "   ค. ปลาสวาย";
        _text4.text = "   ง. ปลายี่สก";

        AskChoiseTrigger._indexChoise = 1;
        AskChoiseTrigger._answer = new int[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    }
}
