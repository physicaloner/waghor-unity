﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

using System.Linq;  
using System.Net;

public class UserScene : MonoBehaviour
{
    private static USER_INFO _USER_INFO;

    public Toggle toggle_1;
    public Toggle toggle_2;
    public Toggle toggle_3;
    public Toggle toggle_4;
    public Toggle toggle_5;
    public Toggle toggle_6;
    public Toggle toggle_7;
    public Toggle toggle_8;
    public Toggle toggle_9;
    public Toggle toggle_10;

    public Button btnNext;

    public void GoToNext_1(string title)
    {
        _USER_INFO = new USER_INFO();

        if (toggle_1 != null && toggle_1.isOn)
            _USER_INFO.CHOISE_1 = 1;
        if (toggle_2 != null && toggle_2.isOn)
            _USER_INFO.CHOISE_1 = 2;
        if (toggle_3 != null && toggle_3.isOn)
            _USER_INFO.CHOISE_1 = 3;
        if (toggle_4 != null && toggle_4.isOn)
            _USER_INFO.CHOISE_1 = 4;
        if (toggle_5 != null && toggle_5.isOn)
            _USER_INFO.CHOISE_1 = 5;
        if (toggle_6 != null && toggle_6.isOn)
            _USER_INFO.CHOISE_1 = 6;
        if (toggle_7 != null && toggle_7.isOn)
            _USER_INFO.CHOISE_1 = 7;
        if (toggle_8 != null && toggle_8.isOn)
            _USER_INFO.CHOISE_1 = 8;
        if (toggle_9 != null && toggle_9.isOn)
            _USER_INFO.CHOISE_1 = 9;
        if (toggle_10 != null && toggle_10.isOn)
            _USER_INFO.CHOISE_1 = 10;

        if (_USER_INFO.CHOISE_1 == 0)
            return;

        Debug.Log($"Scene 1 = {_USER_INFO.CHOISE_1}");

        SceneManager.LoadScene(title, LoadSceneMode.Single);
    }

    public void GoToNext_2(string title)
    {
        if (toggle_1 != null && toggle_1.isOn)
            _USER_INFO.CHOISE_2 = 1;
        else if (toggle_2 != null && toggle_2.isOn)
            _USER_INFO.CHOISE_2 = 2;
        else if (toggle_3 != null && toggle_3.isOn)
            _USER_INFO.CHOISE_2 = 3;
        else if (toggle_4 != null && toggle_4.isOn)
            _USER_INFO.CHOISE_2 = 4;
        else if (toggle_5 != null && toggle_5.isOn)
            _USER_INFO.CHOISE_2 = 5;
        else if (toggle_6 != null && toggle_6.isOn)
            _USER_INFO.CHOISE_2 = 6;
        else if (toggle_7 != null && toggle_7.isOn)
            _USER_INFO.CHOISE_2 = 7;
        else if (toggle_8 != null && toggle_8.isOn)
            _USER_INFO.CHOISE_2 = 8;
        else if (toggle_9 != null && toggle_9.isOn)
            _USER_INFO.CHOISE_2 = 9;
        else if (toggle_10 != null && toggle_10.isOn)
            _USER_INFO.CHOISE_2 = 10;

        if (_USER_INFO.CHOISE_2 == 0)
            return;

        SceneManager.LoadScene(title, LoadSceneMode.Single);
    }

    public void GoToNext_3(string title)
    {
        if (toggle_1 != null && toggle_1.isOn)
            _USER_INFO.CHOISE_3 = 1;
        if (toggle_2 != null && toggle_2.isOn)
            _USER_INFO.CHOISE_3 = 2;
        if (toggle_3 != null && toggle_3.isOn)
            _USER_INFO.CHOISE_3 = 3;
        if (toggle_4 != null && toggle_4.isOn)
            _USER_INFO.CHOISE_3 = 4;
        if (toggle_5 != null && toggle_5.isOn)
            _USER_INFO.CHOISE_3 = 5;
        if (toggle_6 != null && toggle_6.isOn)
            _USER_INFO.CHOISE_3 = 6;
        if (toggle_7 != null && toggle_7.isOn)
            _USER_INFO.CHOISE_3 = 7;
        if (toggle_8 != null && toggle_8.isOn)
            _USER_INFO.CHOISE_3 = 8;
        if (toggle_9 != null && toggle_9.isOn)
            _USER_INFO.CHOISE_3 = 9;
        if (toggle_10 != null && toggle_10.isOn)
            _USER_INFO.CHOISE_3 = 10;

        if (_USER_INFO.CHOISE_3 == 0)
            return;

        SceneManager.LoadScene(title, LoadSceneMode.Single);
    }

    public void GoToNext_4(string title)
    {
        if (toggle_1 != null && toggle_1.isOn)
            _USER_INFO.CHOISE_4 = 1;
        else if (toggle_2 != null && toggle_2.isOn)
            _USER_INFO.CHOISE_4 = 2;
        else if (toggle_3 != null && toggle_3.isOn)
            _USER_INFO.CHOISE_4 = 3;
        else if (toggle_4 != null && toggle_4.isOn)
            _USER_INFO.CHOISE_4 = 4;
        else if (toggle_5 != null && toggle_5.isOn)
            _USER_INFO.CHOISE_4 = 5;
        else if (toggle_6 != null && toggle_6.isOn)
            _USER_INFO.CHOISE_4 = 6;
        else if (toggle_7 != null && toggle_7.isOn)
            _USER_INFO.CHOISE_4 = 7;
        else if (toggle_8 != null && toggle_8.isOn)
            _USER_INFO.CHOISE_4 = 8;
        else if (toggle_9 != null && toggle_9.isOn)
            _USER_INFO.CHOISE_4 = 9;
        else if (toggle_10 != null && toggle_10.isOn)
            _USER_INFO.CHOISE_4 = 10;

        if (_USER_INFO.CHOISE_4 == 0)
            return;

        SceneManager.LoadScene(title, LoadSceneMode.Single);
    }

    public void GoToNext_5(string title)
    {
        if (toggle_1 != null && toggle_1.isOn)
            _USER_INFO.CHOISE_5 = 1;
        if (toggle_2 != null && toggle_2.isOn)
            _USER_INFO.CHOISE_5 = 2;
        if (toggle_3 != null && toggle_3.isOn)
            _USER_INFO.CHOISE_5 = 3;
        if (toggle_4 != null && toggle_4.isOn)
            _USER_INFO.CHOISE_5 = 4;
        if (toggle_5 != null && toggle_5.isOn)
            _USER_INFO.CHOISE_5 = 5;
        if (toggle_6 != null && toggle_6.isOn)
            _USER_INFO.CHOISE_5 = 6;
        if (toggle_7 != null && toggle_7.isOn)
            _USER_INFO.CHOISE_5 = 7;
        if (toggle_8 != null && toggle_8.isOn)
            _USER_INFO.CHOISE_5 = 8;
        if (toggle_9 != null && toggle_9.isOn)
            _USER_INFO.CHOISE_5 = 9;
        if (toggle_10 != null && toggle_10.isOn)
            _USER_INFO.CHOISE_5 = 10;

        if (_USER_INFO.CHOISE_5 == 0)
            return;

        SceneManager.LoadScene(title, LoadSceneMode.Single);
    }

    public void GoToNext_6(string title)
    {
        if (toggle_1 != null && toggle_1.isOn)
            _USER_INFO.CHOISE_6 = 1;
        if (toggle_2 != null && toggle_2.isOn)
            _USER_INFO.CHOISE_6 = 2;
        if (toggle_3 != null && toggle_3.isOn)
            _USER_INFO.CHOISE_6 = 3;
        if (toggle_4 != null && toggle_4.isOn)
            _USER_INFO.CHOISE_6 = 4;
        if (toggle_5 != null && toggle_5.isOn)
            _USER_INFO.CHOISE_6 = 5;
        if (toggle_6 != null && toggle_6.isOn)
            _USER_INFO.CHOISE_6 = 6;
        if (toggle_7 != null && toggle_7.isOn)
            _USER_INFO.CHOISE_6 = 7;
        if (toggle_8 != null && toggle_8.isOn)
            _USER_INFO.CHOISE_6 = 8;
        if (toggle_9 != null && toggle_9.isOn)
            _USER_INFO.CHOISE_6 = 9;
        if (toggle_10 != null && toggle_10.isOn)
            _USER_INFO.CHOISE_6 = 10;

        if (_USER_INFO.CHOISE_6 == 0)
            return;

        SceneManager.LoadScene(title, LoadSceneMode.Single);
    }

    public void GoToNext_7(string title)
    {
        if (toggle_1 != null && toggle_1.isOn)
            _USER_INFO.CHOISE_7 = 1;
        if (toggle_2 != null && toggle_2.isOn)
            _USER_INFO.CHOISE_7 = 2;
        if (toggle_3 != null && toggle_3.isOn)
            _USER_INFO.CHOISE_7 = 3;
        if (toggle_4 != null && toggle_4.isOn)
            _USER_INFO.CHOISE_7 = 4;
        if (toggle_5 != null && toggle_5.isOn)
            _USER_INFO.CHOISE_7 = 5;
        if (toggle_6 != null && toggle_6.isOn)
            _USER_INFO.CHOISE_7 = 6;
        if (toggle_7 != null && toggle_7.isOn)
            _USER_INFO.CHOISE_7 = 7;
        if (toggle_8 != null && toggle_8.isOn)
            _USER_INFO.CHOISE_7 = 8;
        if (toggle_9 != null && toggle_9.isOn)
            _USER_INFO.CHOISE_7 = 9;
        if (toggle_10 != null && toggle_10.isOn)
            _USER_INFO.CHOISE_7 = 10;

        if (_USER_INFO.CHOISE_7 == 0)
            return;

        SceneManager.LoadScene(title, LoadSceneMode.Single);
    }

    public void GoToNext_8(string title)
    {
        if (toggle_1 != null && toggle_1.isOn)
            _USER_INFO.CHOISE_8 = 1;
        if (toggle_2 != null && toggle_2.isOn)
            _USER_INFO.CHOISE_8 = 2;
        if (toggle_3 != null && toggle_3.isOn)
            _USER_INFO.CHOISE_8 = 3;
        if (toggle_4 != null && toggle_4.isOn)
            _USER_INFO.CHOISE_8 = 4;
        if (toggle_5 != null && toggle_5.isOn)
            _USER_INFO.CHOISE_8 = 5;
        if (toggle_6 != null && toggle_6.isOn)
            _USER_INFO.CHOISE_8 = 6;
        if (toggle_7 != null && toggle_7.isOn)
            _USER_INFO.CHOISE_8 = 7;
        if (toggle_8 != null && toggle_8.isOn)
            _USER_INFO.CHOISE_8 = 8;
        if (toggle_9 != null && toggle_9.isOn)
            _USER_INFO.CHOISE_8 = 9;
        if (toggle_10 != null && toggle_10.isOn)
            _USER_INFO.CHOISE_8 = 10;

        if (_USER_INFO.CHOISE_8 == 0)
            return;

        SceneManager.LoadScene(title, LoadSceneMode.Single);
    }

    public void GoToNext_9(string title)
    {
        if (toggle_1 != null && toggle_1.isOn)
            _USER_INFO.CHOISE_9 = 1;
        if (toggle_2 != null && toggle_2.isOn)
            _USER_INFO.CHOISE_9 = 2;
        if (toggle_3 != null && toggle_3.isOn)
            _USER_INFO.CHOISE_9 = 3;
        if (toggle_4 != null && toggle_4.isOn)
            _USER_INFO.CHOISE_9 = 4;
        if (toggle_5 != null && toggle_5.isOn)
            _USER_INFO.CHOISE_9 = 5;
        if (toggle_6 != null && toggle_6.isOn)
            _USER_INFO.CHOISE_9 = 6;
        if (toggle_7 != null && toggle_7.isOn)
            _USER_INFO.CHOISE_9 = 7;
        if (toggle_8 != null && toggle_8.isOn)
            _USER_INFO.CHOISE_9 = 8;
        if (toggle_9 != null && toggle_9.isOn)
            _USER_INFO.CHOISE_9 = 9;
        if (toggle_10 != null && toggle_10.isOn)
            _USER_INFO.CHOISE_9 = 10;

        if (_USER_INFO.CHOISE_9 == 0)
            return;

        SceneManager.LoadScene(title, LoadSceneMode.Single);
    }

    public void GoToNext_10(string title)
    {
        if (toggle_1 != null && toggle_1.isOn)
            _USER_INFO.CHOISE_10 = 1;
        if (toggle_2 != null && toggle_2.isOn)
            _USER_INFO.CHOISE_10 = 2;
        if (toggle_3 != null && toggle_3.isOn)
            _USER_INFO.CHOISE_10 = 3;
        if (toggle_4 != null && toggle_4.isOn)
            _USER_INFO.CHOISE_10 = 4;
        if (toggle_5 != null && toggle_5.isOn)
            _USER_INFO.CHOISE_10 = 5;
        if (toggle_6 != null && toggle_6.isOn)
            _USER_INFO.CHOISE_10 = 6;
        if (toggle_7 != null && toggle_7.isOn)
            _USER_INFO.CHOISE_10 = 7;
        if (toggle_8 != null && toggle_8.isOn)
            _USER_INFO.CHOISE_10 = 8;
        if (toggle_9 != null && toggle_9.isOn)
            _USER_INFO.CHOISE_10 = 9;
        if (toggle_10 != null && toggle_10.isOn)
            _USER_INFO.CHOISE_10 = 10;

        if (_USER_INFO.CHOISE_10 == 0)
            return;

        SceneManager.LoadScene(title, LoadSceneMode.Single);
    }

    public void GoToNext_11(string title)
    {
        if (toggle_1 != null && toggle_1.isOn)
            _USER_INFO.CHOISE_11 = 1;
        if (toggle_2 != null && toggle_2.isOn)
            _USER_INFO.CHOISE_11 = 2;
        if (toggle_3 != null && toggle_3.isOn)
            _USER_INFO.CHOISE_11 = 3;
        if (toggle_4 != null && toggle_4.isOn)
            _USER_INFO.CHOISE_11 = 4;
        if (toggle_5 != null && toggle_5.isOn)
            _USER_INFO.CHOISE_11 = 5;
        if (toggle_6 != null && toggle_6.isOn)
            _USER_INFO.CHOISE_11 = 6;
        if (toggle_7 != null && toggle_7.isOn)
            _USER_INFO.CHOISE_11 = 7;
        if (toggle_8 != null && toggle_8.isOn)
            _USER_INFO.CHOISE_11 = 8;
        if (toggle_9 != null && toggle_9.isOn)
            _USER_INFO.CHOISE_11 = 9;
        if (toggle_10 != null && toggle_10.isOn)
            _USER_INFO.CHOISE_11 = 10;

        if (_USER_INFO.CHOISE_11 == 0)
            return;

        SceneManager.LoadScene(title, LoadSceneMode.Single);
    }

    public void GoToNext_12(string title)
    {
        if (toggle_1 != null && toggle_1.isOn)
            _USER_INFO.CHOISE_12 = 1;
        if (toggle_2 != null && toggle_2.isOn)
            _USER_INFO.CHOISE_12 = 2;
        if (toggle_3 != null && toggle_3.isOn)
            _USER_INFO.CHOISE_12 = 3;
        if (toggle_4 != null && toggle_4.isOn)
            _USER_INFO.CHOISE_12 = 4;
        if (toggle_5 != null && toggle_5.isOn)
            _USER_INFO.CHOISE_12 = 5;
        if (toggle_6 != null && toggle_6.isOn)
            _USER_INFO.CHOISE_12 = 6;
        if (toggle_7 != null && toggle_7.isOn)
            _USER_INFO.CHOISE_12 = 7;
        if (toggle_8 != null && toggle_8.isOn)
            _USER_INFO.CHOISE_12 = 8;
        if (toggle_9 != null && toggle_9.isOn)
            _USER_INFO.CHOISE_12 = 9;
        if (toggle_10 != null && toggle_10.isOn)
            _USER_INFO.CHOISE_12 = 10;

        if (_USER_INFO.CHOISE_12 == 0)
            return;

        SceneManager.LoadScene(title, LoadSceneMode.Single);
    }

    public void GoToNext_13(string title)
    {
        if (toggle_1 != null && toggle_1.isOn)
            _USER_INFO.CHOISE_13 = 1;
        if (toggle_2 != null && toggle_2.isOn)
            _USER_INFO.CHOISE_13 = 2;
        if (toggle_3 != null && toggle_3.isOn)
            _USER_INFO.CHOISE_13 = 3;
        if (toggle_4 != null && toggle_4.isOn)
            _USER_INFO.CHOISE_13 = 4;
        if (toggle_5 != null && toggle_5.isOn)
            _USER_INFO.CHOISE_13 = 5;
        if (toggle_6 != null && toggle_6.isOn)
            _USER_INFO.CHOISE_13 = 6;
        if (toggle_7 != null && toggle_7.isOn)
            _USER_INFO.CHOISE_13 = 7;
        if (toggle_8 != null && toggle_8.isOn)
            _USER_INFO.CHOISE_13 = 8;
        if (toggle_9 != null && toggle_9.isOn)
            _USER_INFO.CHOISE_13 = 9;
        if (toggle_10 != null && toggle_10.isOn)
            _USER_INFO.CHOISE_13 = 10;

        if (_USER_INFO.CHOISE_13 == 0)
            return;

        SceneManager.LoadScene(title, LoadSceneMode.Single);
    }

    public void GoToNext_14(string title)
    {
        if (toggle_1 != null && toggle_1.isOn)
            _USER_INFO.CHOISE_14 = 1;
        if (toggle_2 != null && toggle_2.isOn)
            _USER_INFO.CHOISE_14 = 2;
        if (toggle_3 != null && toggle_3.isOn)
            _USER_INFO.CHOISE_14 = 3;
        if (toggle_4 != null && toggle_4.isOn)
            _USER_INFO.CHOISE_14 = 4;
        if (toggle_5 != null && toggle_5.isOn)
            _USER_INFO.CHOISE_14 = 5;
        if (toggle_6 != null && toggle_6.isOn)
            _USER_INFO.CHOISE_14 = 6;
        if (toggle_7 != null && toggle_7.isOn)
            _USER_INFO.CHOISE_14 = 7;
        if (toggle_8 != null && toggle_8.isOn)
            _USER_INFO.CHOISE_14 = 8;
        if (toggle_9 != null && toggle_9.isOn)
            _USER_INFO.CHOISE_14 = 9;
        if (toggle_10 != null && toggle_10.isOn)
            _USER_INFO.CHOISE_14 = 10;

        if (_USER_INFO.CHOISE_14 == 0)
            return;

        SceneManager.LoadScene(title, LoadSceneMode.Single);
    }

    public void GoToNext_15(string title)
    {
        if (toggle_1 != null && toggle_1.isOn)
            _USER_INFO.CHOISE_15 = 1;
        if (toggle_2 != null && toggle_2.isOn)
            _USER_INFO.CHOISE_15 = 2;
        if (toggle_3 != null && toggle_3.isOn)
            _USER_INFO.CHOISE_15 = 3;
        if (toggle_4 != null && toggle_4.isOn)
            _USER_INFO.CHOISE_15 = 4;
        if (toggle_5 != null && toggle_5.isOn)
            _USER_INFO.CHOISE_15 = 5;
        if (toggle_6 != null && toggle_6.isOn)
            _USER_INFO.CHOISE_15 = 6;
        if (toggle_7 != null && toggle_7.isOn)
            _USER_INFO.CHOISE_15 = 7;
        if (toggle_8 != null && toggle_8.isOn)
            _USER_INFO.CHOISE_15 = 8;
        if (toggle_9 != null && toggle_9.isOn)
            _USER_INFO.CHOISE_15 = 9;
        if (toggle_10 != null && toggle_10.isOn)
            _USER_INFO.CHOISE_15 = 10;

        if (_USER_INFO.CHOISE_15 == 0)
            return;

        SceneManager.LoadScene(title, LoadSceneMode.Single);
    }

    public void GoToNext_16(string title)
    {
        if (toggle_1 != null && toggle_1.isOn)
            _USER_INFO.CHOISE_16 = 1;
        if (toggle_2 != null && toggle_2.isOn)
            _USER_INFO.CHOISE_16 = 2;
        if (toggle_3 != null && toggle_3.isOn)
            _USER_INFO.CHOISE_16 = 3;
        if (toggle_4 != null && toggle_4.isOn)
            _USER_INFO.CHOISE_16 = 4;
        if (toggle_5 != null && toggle_5.isOn)
            _USER_INFO.CHOISE_16 = 5;
        if (toggle_6 != null && toggle_6.isOn)
            _USER_INFO.CHOISE_16 = 6;
        if (toggle_7 != null && toggle_7.isOn)
            _USER_INFO.CHOISE_16 = 7;
        if (toggle_8 != null && toggle_8.isOn)
            _USER_INFO.CHOISE_16 = 8;
        if (toggle_9 != null && toggle_9.isOn)
            _USER_INFO.CHOISE_16 = 9;
        if (toggle_10 != null && toggle_10.isOn)
            _USER_INFO.CHOISE_16 = 10;

        if (_USER_INFO.CHOISE_16 == 0)
            return;

        SceneManager.LoadScene(title, LoadSceneMode.Single);
    }

    public void GoToNext_17(string title)
    {
        if (toggle_1 != null && toggle_1.isOn)
            _USER_INFO.CHOISE_17 = 1;
        if (toggle_2 != null && toggle_2.isOn)
            _USER_INFO.CHOISE_17 = 2;
        if (toggle_3 != null && toggle_3.isOn)
            _USER_INFO.CHOISE_17 = 3;
        if (toggle_4 != null && toggle_4.isOn)
            _USER_INFO.CHOISE_17 = 4;
        if (toggle_5 != null && toggle_5.isOn)
            _USER_INFO.CHOISE_17 = 5;
        if (toggle_6 != null && toggle_6.isOn)
            _USER_INFO.CHOISE_17 = 6;
        if (toggle_7 != null && toggle_7.isOn)
            _USER_INFO.CHOISE_17 = 7;
        if (toggle_8 != null && toggle_8.isOn)
            _USER_INFO.CHOISE_17 = 8;
        if (toggle_9 != null && toggle_9.isOn)
            _USER_INFO.CHOISE_17 = 9;
        if (toggle_10 != null && toggle_10.isOn)
            _USER_INFO.CHOISE_17 = 10;

        if (_USER_INFO.CHOISE_17 == 0)
            return;

        SceneManager.LoadScene(title, LoadSceneMode.Single);
    }

    public void GoToNext_18(string title)
    {
        if (toggle_1 != null && toggle_1.isOn)
            _USER_INFO.CHOISE_18 = 1;
        if (toggle_2 != null && toggle_2.isOn)
            _USER_INFO.CHOISE_18 = 2;
        if (toggle_3 != null && toggle_3.isOn)
            _USER_INFO.CHOISE_18 = 3;
        if (toggle_4 != null && toggle_4.isOn)
            _USER_INFO.CHOISE_18 = 4;
        if (toggle_5 != null && toggle_5.isOn)
            _USER_INFO.CHOISE_18 = 5;
        if (toggle_6 != null && toggle_6.isOn)
            _USER_INFO.CHOISE_18 = 6;
        if (toggle_7 != null && toggle_7.isOn)
            _USER_INFO.CHOISE_18 = 7;
        if (toggle_8 != null && toggle_8.isOn)
            _USER_INFO.CHOISE_18 = 8;
        if (toggle_9 != null && toggle_9.isOn)
            _USER_INFO.CHOISE_18 = 9;
        if (toggle_10 != null && toggle_10.isOn)
            _USER_INFO.CHOISE_18 = 10;

        if (_USER_INFO.CHOISE_18 == 0)
            return;

        SceneManager.LoadScene(title, LoadSceneMode.Single);
    }

    public void GoToNext_19(string title)
    {
        if (toggle_1 != null && toggle_1.isOn)
            _USER_INFO.CHOISE_19 = 1;
        if (toggle_2 != null && toggle_2.isOn)
            _USER_INFO.CHOISE_19 = 2;
        if (toggle_3 != null && toggle_3.isOn)
            _USER_INFO.CHOISE_19 = 3;
        if (toggle_4 != null && toggle_4.isOn)
            _USER_INFO.CHOISE_19 = 4;
        if (toggle_5 != null && toggle_5.isOn)
            _USER_INFO.CHOISE_19 = 5;
        if (toggle_6 != null && toggle_6.isOn)
            _USER_INFO.CHOISE_19 = 6;
        if (toggle_7 != null && toggle_7.isOn)
            _USER_INFO.CHOISE_19 = 7;
        if (toggle_8 != null && toggle_8.isOn)
            _USER_INFO.CHOISE_19 = 8;
        if (toggle_9 != null && toggle_9.isOn)
            _USER_INFO.CHOISE_19 = 9;
        if (toggle_10 != null && toggle_10.isOn)
            _USER_INFO.CHOISE_19 = 10;

        if (_USER_INFO.CHOISE_19 == 0)
            return;

        SceneManager.LoadScene(title, LoadSceneMode.Single);
    }

    public void GoToNext_20(string title)
    {
        if (toggle_1 != null && toggle_1.isOn)
            _USER_INFO.CHOISE_20 = 1;
        if (toggle_2 != null && toggle_2.isOn)
            _USER_INFO.CHOISE_20 = 2;
        if (toggle_3 != null && toggle_3.isOn)
            _USER_INFO.CHOISE_20 = 3;
        if (toggle_4 != null && toggle_4.isOn)
            _USER_INFO.CHOISE_20 = 4;
        if (toggle_5 != null && toggle_5.isOn)
            _USER_INFO.CHOISE_20 = 5;
        if (toggle_6 != null && toggle_6.isOn)
            _USER_INFO.CHOISE_20 = 6;
        if (toggle_7 != null && toggle_7.isOn)
            _USER_INFO.CHOISE_20 = 7;
        if (toggle_8 != null && toggle_8.isOn)
            _USER_INFO.CHOISE_20 = 8;
        if (toggle_9 != null && toggle_9.isOn)
            _USER_INFO.CHOISE_20 = 9;
        if (toggle_10 != null && toggle_10.isOn)
            _USER_INFO.CHOISE_20 = 10;

        if (_USER_INFO.CHOISE_20 == 0)
            return;

        SceneManager.LoadScene(title, LoadSceneMode.Single);
    }

    public void GoToNext_21(string title)
    {
        if (toggle_1 != null && toggle_1.isOn)
            _USER_INFO.CHOISE_21 = 1;
        if (toggle_2 != null && toggle_2.isOn)
            _USER_INFO.CHOISE_21 = 2;
        if (toggle_3 != null && toggle_3.isOn)
            _USER_INFO.CHOISE_21 = 3;
        if (toggle_4 != null && toggle_4.isOn)
            _USER_INFO.CHOISE_21 = 4;
        if (toggle_5 != null && toggle_5.isOn)
            _USER_INFO.CHOISE_21 = 5;
        if (toggle_6 != null && toggle_6.isOn)
            _USER_INFO.CHOISE_21 = 6;
        if (toggle_7 != null && toggle_7.isOn)
            _USER_INFO.CHOISE_21 = 7;
        if (toggle_8 != null && toggle_8.isOn)
            _USER_INFO.CHOISE_21 = 8;
        if (toggle_9 != null && toggle_9.isOn)
            _USER_INFO.CHOISE_21 = 9;
        if (toggle_10 != null && toggle_10.isOn)
            _USER_INFO.CHOISE_21 = 10;

        btnNext.enabled = false;

        if (_USER_INFO.CHOISE_21 == 0)
            return;

        _USER_INFO.UPDATE_DATE = System.DateTime.Now;

        UserSceneAll_POST();
    }

    public void UserSceneAll_POST()
    {
        StartCoroutine(PostUser_WWW());
        //SceneManager.LoadScene("MainScene", LoadSceneMode.Single);
    }

    private IEnumerator PostUser_WWW()
    {
        // Convert object to json here
        var jsonString = JsonUtility.ToJson(_USER_INFO);
        IPAddress[] ipaddress = Dns.GetHostAddresses("WAGHOR-SERVER");  
        IPAddress ip4 = ipaddress.Where(ip => ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork && ip.ToString().IndexOf("192") != -1).FirstOrDefault();
        var url = $"http://{ip4.ToString()}:5000/api/USER_INFO/CREATE";
        //var url = "http://localhost:5000/api/USER_INFO/CREATE";
        var request = new UnityWebRequest(url, "POST");
        byte[] bodyRaw = System.Text.Encoding.ASCII.GetBytes(jsonString);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");

        yield return request.SendWebRequest();

        Debug.Log("Status Code: " + request.responseCode);
        _USER_INFO = new USER_INFO();

        SceneManager.LoadScene("MainScene", LoadSceneMode.Single);
    }

    //void Test()
    //{
    //    if (toggle_1 != null && toggle_1.isOn)
    //        _USER_INFO.CHOISE_15 = 1;
    //    if (toggle_2 != null && toggle_2.isOn)
    //        _USER_INFO.CHOISE_15 = 2;
    //    if (toggle_3 != null && toggle_3.isOn)
    //        _USER_INFO.CHOISE_15 = 3;
    //    if (toggle_4 != null && toggle_4.isOn)
    //        _USER_INFO.CHOISE_15 = 4;
    //    if (toggle_5 != null && toggle_5.isOn)
    //        _USER_INFO.CHOISE_15 = 5;
    //    if (toggle_6 != null && toggle_6.isOn)
    //        _USER_INFO.CHOISE_15 = 6;
    //    if (toggle_7 != null && toggle_7.isOn)
    //        _USER_INFO.CHOISE_15 = 7;
    //    if (toggle_8 != null && toggle_8.isOn)
    //        _USER_INFO.CHOISE_15 = 8;
    //    if (toggle_9 != null && toggle_9.isOn)
    //        _USER_INFO.CHOISE_15 = 9;
    //    if (toggle_10 != null && toggle_10.isOn)
    //        _USER_INFO.CHOISE_15 = 10;
    //}
}
