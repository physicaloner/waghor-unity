﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score1Loader : MonoBehaviour
{
    public Text textScore;

    // Start is called before the first frame update
    void Start()
    {
        textScore.text = $"คุณได้คะแนนในแบบทดสอบนี้ {AskScene1._USER_ASK.SCORE_1} ข้อ";
    }
}
