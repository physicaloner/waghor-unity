﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score3Loader : MonoBehaviour
{
    public Text textScore;

    // Start is called before the first frame update
    void Start()
    {
        textScore.text = $"คุณได้คะแนนในแบบทดสอบนี้ {AskScene1._USER_ASK.SCORE_3} ข้อ";
    }
}
